import { Component, OnInit } from '@angular/core';
import { HeroesService } from '../services/heroes.service';


@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  constructor(private AboutSvc:HeroesService) { }

  dataAbout: any = [];


  ngOnInit(): void {
  this.AboutSvc.getAbout().subscribe((data:any) =>{
    console.log(data);
    this.dataAbout = data;
  });
  }

}

