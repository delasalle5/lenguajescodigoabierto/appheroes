import { Component, OnInit } from '@angular/core';
import { HeroesService } from '../services/heroes.service';

@Component({
  selector: 'app-dc',
  templateUrl: './dc.component.html',
  styleUrls: ['./dc.component.css']
})
export class DcComponent implements OnInit {

  constructor(private heroesDCSvc:HeroesService) { }

  dataDc: any = [];
  

  ngOnInit(): void {
  
  this.heroesDCSvc.getDCHeroes().subscribe((data:any) =>{
  console.log(data);
  this.dataDc = data.data;


});
}

}